# Vagrant-Ansible-Target-VM

## Setup Windows 7 Workstation as a RHEL-Similar enough Ansible Deployment Target

### Install Chocolatey
In cmd:
```
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

### Install babun
In cmd:
```
choco install babun
```

### Generate SSH key pairs between your computer and the GitHub Server
This is covered within the scope of setting up your GitHub account.
If you're unable to locate this information, go to your Profile Settings by
clicking on the top right corner icon. Click "Settings" at the top, then "SSH & GPG Keys"
on the left side.

In babun:
```
ssh-keygen -t rsa -C "MyWorkstationBabun"
cat ~/.ssh/id_rsa.pub
chmod 600 ~/.ssh/id_rsa
```
Copy the text generated and put into the SSH key field on the previous screen.
Resume through the remaining prompts. Once done, you should be able to git clone
repositories.

### Install VirtualBox
In cmd:
```
choco install virtualbox
```

### Install Vagrant
In cmd:
```
choco install vagrant
```

### Restart your computer
In cmd:
```
shutdown /r
```

### Create & Move to the location where you want to create the vagrant box
In babun:
```
mkdir /c/Users/$USERNAME/Vagrant
cd /c/Users/$USERNAME/Vagrant
```

### Download VM
In babun:
```
git clone git@github.com:Rakaim/vagrant-ansible-target-vm.git .
vagrant box add centos7 https://github.com/holms/vagrant-centos7-box/releases/download/7.1.1503.001/CentOS-7.1.1503-x86_64-netboot.box --insecure
```

### Install Internal Certs
:notebook: Only do this step if you're behind a proxy
```
mkdir internal-repos/
# Copy your internal certificates here
```

### Construct VM
In babun:
```
vagrant up
```

### ssh to VM
In babun:
```
vagrant ssh
```

___


# About this VM

### Features
This virtual machine:
1. hijacks port 22 (ssh) of the host it's running on.
2. ssh from another machine. Example:
```
ssh vagrant@yourcomputer.domain.com
```
2. Installs python from yum

# Credit
Kurt Bomya
