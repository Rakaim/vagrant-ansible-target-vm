#!/bin/bash
#This will provision the VM during 'vagrant up'

yum install -y python
yum install -y ca-certificates
update-ca-trust force-enable
sudo cp /vagrant/*.pem /etc/pki/ca-trust/source/anchors
update-ca-trust extract
rm /etc/resolv.conf
cp /vagrant/resolv.conf /etc/resolv.conf

rm -rf /etc/yum.repos.d
mkdir /etc/yum.repos.d
sudo cp /vagrant/internal-repos/*.repo /etc/yum.repos.d
